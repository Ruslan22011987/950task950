﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace XUnitTestHotelServis.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AddDBEntrys",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    nameMetod = table.Column<string>(nullable: true),
                    result = table.Column<string>(nullable: true),
                    startMetod = table.Column<string>(nullable: true),
                    stopMetod = table.Column<string>(nullable: true),
                    leadTime = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AddDBEntrys", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AddDBEntrys");
        }
    }
}
